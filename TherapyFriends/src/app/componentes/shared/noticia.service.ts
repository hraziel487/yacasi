import { Injectable } from '@angular/core';
import { Noticias } from './Noticias';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class NoticiaService {
noticiasListRef: AngularFireList<any>;
noticiasRef: AngularFireObject<any>;

constructor(private db: AngularFireDatabase) { }
 // Create
 createNota(nota: Noticias) {
  return this.noticiasListRef.push({
    nombre: nota.nombre,
    email: nota.email,
    telefono: nota.telefono,
    descripcion: nota.descripcion
  });
}

// Get Single
getNota(id: string) {
  this.noticiasRef = this.db.object('/noticia/' + id);
  return this.noticiasRef;
}

// Get List
getNotasList() {
  this.noticiasListRef = this.db.list('/noticia');
  return this.noticiasListRef;
}

// Update
updateNota(id, nota: Noticias) {
  return this.noticiasRef.update({
    nombre: nota.nombre,
    email: nota.email,
    telefono: nota.telefono,
    descripcion: nota.descripcion
  });
}

// Delete
deleteNota(id: string) {
  this.noticiasRef = this.db.object('/noticia/' + id);
  this.noticiasRef.remove();
}
}

