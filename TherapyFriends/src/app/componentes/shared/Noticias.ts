export class Noticias {
    $key: string;
    nombre: string;
    email: string;
    telefono: number;
    descripcion: string;
}