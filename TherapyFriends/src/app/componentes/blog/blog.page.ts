import { Component, OnInit } from '@angular/core';
import { NoticiaService } from '../shared/noticia.service';
import { Noticias } from '../shared/Noticias';
import { MenuController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.page.html',
  styleUrls: ['./blog.page.scss'],
})
export class BlogPage implements OnInit {
  Notas = [];

  constructor(
    private aptService: NoticiaService,
    private menu: MenuController,
    public afAuth: AngularFireAuth
   
  ) { }
  ngOnInit() {
    this.fetchBookings();
    const notasRes = this.aptService.getNotasList();
    notasRes.snapshotChanges().subscribe(res => {
      this.Notas = [];
      res.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        this.Notas.push(a as Noticias);
      });
    });
  }

  fetchBookings() {
    this.aptService.getNotasList().valueChanges().subscribe(res => {
      console.log(res);
    });
  }

  deleteNota(id) {
    console.log(id);
    if (window.confirm('Seguro que quieres eliminar esta nota?')) {
      this.aptService.deleteNota(id);
    }
  }
}
