import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from "@ionic/angular";
import { $ } from 'protractor';

@Component({
  selector: 'app-imagen-modal',
  templateUrl: './imagen-modal.page.html',
  styleUrls: ['./imagen-modal.page.scss'],
  template: `
   <ion-content fullscreen>

  <ion-item id="item" class="botones">
    <ion-button (click)="cerrarModal()" fill="clear">
      <ion-icon name="close"></ion-icon>
    </ion-button>
  </ion-item>


  <ion-slides pager="true"   [options]="slideOpts" #slides>
    <ion-slide *ngFor="let imagen of imagenes">
        <img src="assets/images/galeria/{{imagen}}" alt="">
    </ion-slide>

  </ion-slides>


  

</ion-content>
`
  
})
export class ImagenModalPage implements OnInit {

  indice: number;
  
  @ViewChild('slides', {static: true}) slides : IonSlides;


  imagenes = [
    'amigo.jpg',
    'chica.jpg',
    'Dia.jpg',
    'Diagnostico.jpg',
    'Madre.jpg',
    'Niña.jpg',
    'Pata.jpg',
    'Perro.jpg',
  ];
  constructor(private modalCtrl : ModalController, private navparams : NavParams) {
       this.indice = this.navparams.get('index');
    console.log(this.indice)
     }
     ngOnInit() {
      this.slides.slideTo(this.indice,0)
    }
  
    cerrarModal(){
      this.modalCtrl.dismiss();
    }
  
}
