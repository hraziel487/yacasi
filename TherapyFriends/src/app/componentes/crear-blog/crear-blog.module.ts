import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearBlogPageRoutingModule } from './crear-blog-routing.module';

import { CrearBlogPage } from './crear-blog.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    CrearBlogPageRoutingModule
  ],
  declarations: [CrearBlogPage]
})
export class CrearBlogPageModule {}
