import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NoticiaService } from '../shared/noticia.service';

@Component({
  selector: 'app-crear-blog',
  templateUrl: './crear-blog.page.html',
  styleUrls: ['./crear-blog.page.scss'],
})
export class CrearBlogPage implements OnInit {
  noticiaForm: FormGroup;

  constructor(
    private aptService: NoticiaService,
    private router: Router,
    public fb: FormBuilder
  ) { }

  ngOnInit() {
    this.noticiaForm = this.fb.group({
      nombre: [''],
      email: [''],
      telefono: [''],
      descripcion: ['']
    });
  }

  formSubmit() {
    if (!this.noticiaForm.valid) {
      return false;
    } else {
      this.aptService.createNota(this.noticiaForm.value).then(res => {
        console.log(res);
        this.noticiaForm.reset();
        this.router.navigate(['/home']);
      })
        .catch(error => console.log(error));
    }
  }
}

