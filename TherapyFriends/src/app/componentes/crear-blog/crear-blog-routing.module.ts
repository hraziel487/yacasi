import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearBlogPage } from './crear-blog.page';

const routes: Routes = [
  {
    path: '',
    component: CrearBlogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearBlogPageRoutingModule {}
