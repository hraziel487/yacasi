import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private menu: MenuController) { }
  mapa: mapboxgl.Map;
  ngOnInit() {
    mapboxgl.accessToken = environment.mapboxKey;
    this.mapa = new mapboxgl.Map({
    container: 'mapa-mapbox', // container id
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [-86.8288344, 21.164339], // starting position
    zoom: 11  // starting zoom
    });
    this.MarcadorFundacion_Jorge_Alann(-86.8420996, 21.1481588);
    this.MarcadorAstra(-86.8365571, 21.1646507);
    this.MarcadorBLUEAUTISMOyASPERGER(-86.8734363, 21.1461512);
    this.MarcadorProNiños(-86.8734363, 21.146153);
    this.Buscador();
    this.Localizacion();
    this.Controles();
  }

  MarcadorAstra(lng: number, lat: number) {
    const popup = new mapboxgl.Popup({ offset: 25 }).setText(
      'Astra, AC - Asociación de Ayuda a Niños con Trastornos en el Desarrollo, A.C.'
      );
    const el = document.createElement('div');
    el.id = 'marker';
    const marker = new mapboxgl.Marker({
      draggable: true
      })
      .setLngLat([lng, lat])
      .setPopup(popup)
      .addTo(this.mapa);
    marker.on('dragend', () => {
        console.log( marker.getLngLat() );
      });
    }// AQUI TERMINA EN MARCADOR
    MarcadorFundacion_Jorge_Alann(lng: number, lat: number) {
      const popup = new mapboxgl.Popup({ offset: 25 }).setText(
        'Fundacion Jorge Alann Ayuda a Niños con Trastornos en el Desarrollo.'
        );
      const el = document.createElement('div');
      el.id = 'marker';
      const marker = new mapboxgl.Marker({
        draggable: true
        })
        .setLngLat([lng, lat])
        .setPopup(popup)
        .addTo(this.mapa);
      marker.on('dragend', () => {
          console.log( marker.getLngLat() );
        });
      }// AQUI TERMINA EN MARCADOR

        MarcadorBLUEAUTISMOyASPERGER(lng: number, lat: number) {
          const popup = new mapboxgl.Popup({ offset: 25 }).setText(
            'Centro Especializado en Autismo BLUEAUTISMO y ASPERGER'
            );
          const el = document.createElement('div');
          el.id = 'marker';
          const marker = new mapboxgl.Marker({
            draggable: true
            })
            .setLngLat([lng, lat])
            .setPopup(popup)
            .addTo(this.mapa);
          marker.on('dragend', () => {
              console.log( marker.getLngLat() );
            });
          }// AQUI TERMINA EN MARCADOR

          MarcadorProNiños(lng: number, lat: number) {
            const popup = new mapboxgl.Popup({ offset: 25 }).setText(
              'Asociacion Pro Niños Excepcionales A.C.'
              );
            const el = document.createElement('div');
            el.id = 'marker';
            const marker = new mapboxgl.Marker({
              draggable: true
              })
              .setLngLat([lng, lat])
              .setPopup(popup)
              .addTo(this.mapa);
            marker.on('dragend', () => {
                console.log( marker.getLngLat() );
              });
            }// AQUI TERMINA EN MARCADOR

      Buscador() {
      const geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        marker: {
        color: 'orange'
     },
     mapboxgl
    });
      this.mapa.addControl(geocoder);
     } // Aqui termina el buscador

     Localizacion() {
      this.mapa.addControl(
        new mapboxgl.GeolocateControl({
        positionOptions: {
        enableHighAccuracy: true
        },
        trackUserLocation: true
        })
        );
     }
     Controles() {
     this.mapa.addControl(new mapboxgl.NavigationControl());
  }
}

